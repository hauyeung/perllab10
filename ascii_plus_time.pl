#!/usr/bin/perl -w
use strict;
use warnings;
use POSIX;
my @nums = ();
for (my $x=32; $x<=126; $x++)
{
	push(@nums, $x)
}
my $time = localtime(time());
print substr($time,0, length($time)-8)."\n";
foreach my $y(@nums)
{
	printf "%f ",$y;
	printf "%x ",$y;
	printf "%o ",$y; 
	printf "%b ",$y;
	print chr($y)." ";
	print "\n";
}